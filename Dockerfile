FROM mikefarah/yq:4 AS yq4
FROM zenika/alpine-chrome:124-with-puppeteer-xvfb AS base

# Identify the maintainer of an image
LABEL maintainer="pierre@gronlier.fr"

COPY --from=yq4 "/usr/bin/yq" "/usr/local/bin/yq"

# system package
USER root
RUN apk update
RUN apk add --update --upgrade --no-cache python3 py3-pip git rsync exiftool

# python
COPY ./requirements.txt .
RUN pip install -Ur requirements.txt --break-system-packages
RUN rm requirements.txt

# node
USER chrome

# WORKDIR value from the base image
ENV NODE_PATH=/usr/src/app/node_modules
# WORKDIR /usr/src/app/
# COPY ./package.json .
# RUN npm install

# copy all template files
COPY ./document_template /document_template/
CMD ["/bin/sh"]
