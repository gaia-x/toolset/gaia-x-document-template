# Gaia-X document template

Docker image with the binaries and toolsets only.

Templates and extra files are located in the document repositories.

## Release process

Assuming that you want to generate a document for a repository named `targetRepo`

1. In the `targetRepo`, check if the `.gitlab-ci.yml` file is up to date.
You can either:
- compare it with the `test` job from the current [`.gitlab-ci.yml`](.gitlab-ci.yml) file 
- use the provided `.gitlab-ci-template.yml` provided in this repository by including using this `.gitlab-ci.yml`:
````yaml
include:
  - project: 'gaia-x/toolset/gaia-x-document-template'
    ref: 'main'
    file: '/.gitlab-ci-template.yml'
````

2. In the `targetRepo`, verify that the pipelines are successful and click on the name of the job generating the content.
In the picture below, the name is `build`
![alt text](readme-pipeline.png)
3. In the `targetRepo`, in the latest successful `build:html` job, download the artefact as archive.
The archive contains under a `public/` folder both the HTML and PDF versions of the document.
![alt text](readme-artefact.png)
4. In the <https://docs.gaia-x.eu> web hosting, upload the archive under `www/docs/<the matching folder>`.

## Dev

The `.gitlab-ci.yml` is inspired from <https://docs.gitlab.com/ee/user/packages/container_registry/build_and_push_images.html#container-registry-examples-with-gitlab-cicd>

For local setup, check <https://github.com/firecow/gitlab-ci-local/tree/master/examples/docker-in-docker-build-with-local-registry>

Setup a local registry

```shell
docker network create gitlab-ci-local-registry
docker run -d -p 127.0.0.1:5000:5000 --network gitlab-ci-local-registry --name gitlab-ci-local-registry registry:2
```

In `.gitlab-ci-local-env`

```shell title=".gitlab-ci-local-env"
PRIVILEGED=true
ULIMIT=8000:16000
VOLUME=certs:/certs/client
VARIABLE="DOCKER_TLS_CERTDIR=/certs"
```

### For `build` and `release` steps

```shell
gitlab-ci-local --variable CI_REGISTRY="gitlab-ci-local-registry:5000" --network gitlab-ci-local-registry build release
```

### For `test` steps

```shell
gitlab-ci-local --variable CI_REGISTRY="127.0.0.1:5000" --pull-policy=always test
```
