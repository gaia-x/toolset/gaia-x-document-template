# Test Emoji

## Design Principles for Gaia-X Standard Compliance and Labels

The Gaia-X Compliance is designed using a set of core principles, starting from the Standard Compliance scheme, which is refined in the Label schemes.

| property                                 | Standard Compliance | Level 1 | Level 2 | Level 3 |
|------------------------------------------|------------|---------|---------|---------|
| Declaration of Service or Product        | ✔️         | ✔️      | ✔️     | ✔️      |
| Signed with verified method (e.g. eIDAS) | ✔️         | ✔️      | ✔️     | ✔️      |
| Automated validation by GXDCH            | ✔️         | ✔️      | ✔️     | ✔️      |
| Automated verification by GXDCH\*         | ✔️         | ✔️      | ➕      | ➕     |
| Data Exchange Policies                   | ✔️         | ✔️      | ✔️      | ✔️     |
| Certified Label Logo                     |            | ✔️      | ✔️      | ✔️     |
| Data protection by EU legislation        |            | ✔️      | ✔️      | ✔️     |
| Manual verification by CAB               |            |         | ✔️      | ✔️      |
| Provider Headquarter within EU           |            |         |         | ✔️      |

\*: *not all criteria can be automated.

➕: means automated verification of the evidence issuer (Standard & CAB)
