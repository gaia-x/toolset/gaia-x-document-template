# Test Maths & Plot

Extracted from <https://docs.gaia-x.eu/technical-committee/architecture-document/24.04/annex_trust_indexes/>

As described in the Conformity Schema section, the ecosystem authority can define different assessment scheme, but those:

- Don’t automatically adapt to the market which always evolves faster than the rules.
- Don’t capture the subtleties of organisational and semantic interoperability complexity, _de facto_ lowering the interoperability to the basic commonly conceded denominators.

To address this challenge, additional scoring tools named Trust Indexes are developed as Veracity $T_V$, Transparency $T_T$, Composability $T_C$ and Semantic-Match $T_{SM}$ indexes.


!!! example


    Those indexes enable the ecosystem parties to:

    - As a consumer to compare objects or offerings with a more granular ranking that wouldn’t necessarily fit into one of the predefined conformity schemes.
    
    ??? example

        Given 2 compliance scheme, one built on top of the other, a service offering compliant with the first one but not second one will be assessed with a score $r_{SO}$ in the range $1.0$ to $2.0$ excluded, as in $r_{SO} \in [1.0, 2.0[$

    - The providers to compare their offering with existing ones and help them to improve their interoperability.
    
    ??? example
        As a provider _"is my service interoperable and composable with 10% or 90% of the other offerings in the ecosystem catalogues ?"_

The proposed Trust Indexes $T_V,T_T,T_C,T_{SM}$ are meant to be used by all parties - licensor, licensee, producer, provider, consumer, $\dots$ - as a measure of distance for interoperability and trust with regards to the other offerings in the ecosystem catalogues.
The intent being that the ecosystem helps the market by providing measurement tools and let the market actors themselves to converge to an optimum solution, similar to [gradient descent](https://en.wikipedia.org/wiki/Gradient_descent) algorithms where an error metric is given and a process iteratively applied converge to an optimum solution.

!!! example
    
    A provider that declares its services or products by providing only the bare minimum information without machine readable claims nor policies may still meet the mandatory ecosystem compliance criteria but will have a low interoperability score with other offerings from the ecosystem catalogues and it would be in the provider’s own interest to improve the quality and quantity of the provided information.

The Trust Index $T$ is a function of several sub-indexes, all using VC as inputs. The output is a number in $\mathbb{R}$.

$$ T: T_V, T_T, T_C, T_{SM} \rightarrow \mathbb{R}$$

with each sub-index $T_i$ as $T_i: \texttt{\{VCs\}} \rightarrow \mathbb{R}$
<!-- using VCs as small hack due to a conflict between arithmetex and abbr extensions -->

!!! tip "Sample code included in the source of this page"
    All the graphs in this page are dynamically computed in Javascript.

    If you are looking for an index implementation, look at the source code of this page.

## Sub-Indexes

### Veracity

The Veracity index $T_V$ is a function of the $n$ chains of the public keys from the issuer to the Trust Anchor.

$$\forall C_k \in \{Chains\}, T_V = \frac{1}{n}\sum_{k=1}^{n} \alpha^{length(C_k)-1}$$

!!! tip
    $\alpha = 0.9$ is recommended to keep a meaningful $T_V$ value even with long machine-to-machine chains.

???example "Evolution of the index with $\alpha$"
    Smaller is $\alpha$, more aggressive is the index with regards to long chains.
    <div id="veracity_fct" style="width:600px;height:250px;"></div>

<script>
    window.addEventListener('load', function() {
        let x = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
        Plotly.newPlot(document.getElementById('veracity_fct'), [
            {name: 'a = 0.9', x: x,  y: x.map((x) => Math.pow(0.9, x-1)), mode: 'lines+markers'},
            {name: 'a = 0.7', x: x,  y: x.map((x) => Math.pow(0.7, x-1)), mode: 'lines+markers', line: {dash: 'dot', width: 2}},
            {name: 'a = 0.5', x: x,  y: x.map((x) => Math.pow(0.5, x-1)), mode: 'lines+markers', line: {dash: 'dot', width: 2}},
            {name: 'a = 0.2', x: x,  y: x.map((x) => Math.pow(0.2, x-1)), mode: 'lines+markers', line: {dash: 'dot', width: 2}},
        ],
        {
            margin: {t: 0},
            xaxis:{title: {text: "Keychain length"}},
            yaxis:{title: {text: "Index"}},
            annotations: [
                {x: x.at(0), y: 1,  text: 'the Trust Anchor is the Issuer', xanchor: 'left'},
                {x: x.at(-1), y: Math.pow(0.9, x.at(-1)-1),  text: 'converge to 0'}
            ]
        });
    }, false);
</script>

**If** there is one chain **then** the value is 1 **else** more chains there are, higher :octicons-arrow-up-right-24: is the veracity index.

!!! example
    For a given claim, the number of signatures on that claim (root → … → leaf → VC → **subject** ← VC ← leaf ← … ← root)

    ```mermaid
    flowchart LR
        keychain1[cert #1]
        keychain2[cert #2]
        keychain3[cert #3]
        keychainA[cert #A]
        keychainB[cert #B]
        vc1(credential #1)
        vc2(credential #2)
        cs([subject])
        keychain1 -- issues --> keychain2 -- issues --> keychain3 -- signs --> vc1 -- refers to --> cs
        keychainA -- issues --> keychainB -- signs ---> vc2 -- refers to --> cs
    ```


**If** the issuer is the Trust Anchor **then** the value is 1 **else** longer the chain is, lower :octicons-arrow-down-right-24: is the veracity index.

!!! example
    For a given claim, the length of the signing key chain (root cert → intermediate cert → … → leaf → VC → **subject**)

    ```mermaid
    flowchart LR
        root1[root cert]
        interm1[intermediate cert #k]
        leaf1[leaf cert]
        vc1(credential #1)
        cs([subject])

        root1 -- issues --> interm1 -- issues --> leaf1 -- signs --> vc1 -- refers to --> cs
    ```

### Transparency

The goal of the transparency index is to reward parties exposing information.

!!! warning "Transparency vs Compliance"
    The transparency index is not linked to the notion of compliance.
    It's possible to have a high transparency index and not being compliant, because mandatory properties are missing or with a wrong format.

!!! warning "Transparency vs VC type"
    The transparency index can only be compared across VC of the **same** type.

The transparency index is based on the number of exposed properties and their cardinality.

$T_T$ is computed in a two steps approach:

1. $T_{T_n}$ values for each VC $n$ of the graph $G$.
2. an overall $T_T$ value from the previous $T_{T_n}$ values, for the VC $n_0$ representing the object of interest.

$T_T$ and $T_{T_n}$ are characterised by:

- $0 \le T_T \le 1$
- $T_T = 0$ when $|\{p\}|=0$, ie the cardinality of the set $\{p\}$ of property $p$ is $0$, ie no property are filled in.
- $\lim_{|\{p\}| \to +\infty} T_T = 1$ when more properties $p$ are filled in.

!!! example
    Example of the same offering - an API endpoint returning a fortune from the BSD packet [fortune](https://en.wikipedia.org/wiki/Fortune_(Unix)) - with an increasing :octicons-arrow-up-right-24: transparency index, $T_{T_2} > T_{T_1} > T_{T_0}$

    === "Minimun requirement $T_{T_0}$"
        ```mermaid
        flowchart TB
            so1{{Fortune teller}}
            part1[Provider 1]
            so1 -- providedBy --> part1
        ```

        **Service Offering**

        ```yaml
        name: Fortune teller
        description: API to randomly return a fortune
        providedBy: url(provider1)
        termsAndConditions:
        - https://some.url.for.terms.and.condition.example.com
        ```

        **Provider 1**

        ```yaml
        registrationNumber: FR5910.899103360
        headquarterAddress:
        country: FR
        legalAddress:
        country: FR
        ```

    === "Improved requirement $T_{T_1} > T_{T_0}$"
        ```mermaid
        flowchart TB
            so1{{Fortune teller}}
            part1[Provider 1]
            vr1[\Software 1\]

            so1 -- providedBy --> part1
            so1 -- aggregationOf --> vr1
            vr1 -- copyrightOwnedBy --> part1 
        ```

        **Service Offering**

        ```yaml
        name: Fortune teller
        description: API to randomly return a fortune
        providedBy: url(provider1)
        aggregationOf:
        - url(software1)
        termsAndConditions:
        - https://some.url.for.terms.and.condition.example.com
        ```

        **Software 1**

        ```yaml
        name: api software
        copyrightOwnedBy:
        - url(provider1)
        license:
        - EPL-2.0
        ```

    === "Improved requirement $T_{T_2} > T_{T_1}$"
        ```mermaid
        flowchart TB
            so1{{Fortune Teller}}
            part1[Provider 1]
            part2[Participant 2]
            part3[Participant 3]
            vr1[\Software 1/]
            vr2[\DataSet\]
            pr1[/Datacenter\]

            so1 -- providedBy --> part1
            so1 -- aggregationOf --> vr1 & vr2 & pr1
            vr1 -- copyrightOwnedBy --> part1
            vr1 -- tenantOwnedByBy --> part1
            vr1 -- maintainedBy --> part1

            vr2 -- copyrightOwnedBy --> part3

            pr1 -- maintainerBy --> part2
        ```

        **Service Offering**

        ```yaml
        name: Fortune teller
        description: API to randomly return a fortune
        providedBy: url(provider1)
        aggregationOf:
        - url(software1)
        - url(dataset1)
        - url(datacenter1)
        termsAndConditions:
        - https://some.url.for.terms.and.condition.example.com
        policies:
        - type: opa
            content: |-
            package fortune
            allow = true {
                input.method = "GET"
            }
        ```

        **API 1**

        ```yaml
        name: api software
        maintainedBy:
        - url(provider1)
        tenantOwnedByBy:
        - url(provider1)
        copyrightOwnedBy:
        - url(provider1)
        license:
        - EPL-2.0
        ```

        **Dataset 1**

        ```yaml
        name: fortune dataset
        copyrightOwnedBy:
        - name: The Regents of the University of California
            registrationNumber: C0008116
            headquarterAddress:
            state: CA
            country: USA
            legalAddress:
            state: CA
            country: USA
        license:
        - BSD-3
        - https://metadata.ftp-master.debian.org/changelogs//main/f/fortune-mod/fortune-mod_1.99.1-7.1_copyright
        ```

        **Participant 2**

        ```yaml
        name: Cloud Service Provider
        registrationNumber: FR5910.424761419
        headquarterAddress:
        country: FR
        legalAddress:
        country: FR
        ```

        **Datacenter 1**

        ```yaml
        name: datacenter
        maintainedBy: url(participant2)
        location:
        - country: FR
        ```


#### For each VC of the graph

To meet the objectives of the Transparency Index set in the previous section, each VC with its associated SHACL shapes must be analysed as follow:

- with $countTotal(n)$ the number of available properties for a VC $n$, every property $p$ is given a weight $w_p$.

$$w_p = \frac{1}{countTotal}$$

- each property $p$ is given a value $v_p$ depending of its cardinality and the number of given values $c$

$$\forall p \in n, T_{T_n} = \sum_p w_p v_p$$

!!! example
    for a VC with 5 properties, each property $p$ has a weight $w_p = 0.2$

**For a cardinality with known boundaries `n..m`**

$$v_p = \begin{cases}
    0,              & \text{if } c \le 0 \\
    \frac{c}{m},& \text{if } 0 \lt c \lt m \\
    1,              & \text{if } c \ge m
\end{cases}$$

??? example "Example with a known maximum"
    <div id="transparency_node_fct_bounded" style="width:600px;height:250px;"></div>

<script>
    function property_value_bounded(c, m) {
        if (c <= 0)
            return 0;
        if (c >= m)
            return 1;
        return c/m;
    }
    window.addEventListener('load', function() {
        // example for a property with max 6 values
        let m = 6;
        let x = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    	Plotly.newPlot(document.getElementById('transparency_node_fct_bounded'), [
            {x: x,  y: x.map(x => property_value_bounded(x, m)), mode: 'lines+markers'},
            ],
        {
            margin: {t: 0},
            xaxis:{title: {text: `Example with m = ${m}`}},
            yaxis:{title: {text: "property value"}},
            annotations: [
                {x: m, y: 1,  text: 'm'},
            ]
        });
    }, false);
</script>

**For a cardinality with an unknow upper boundary `n..*`**

$$v_p = \begin{cases}
    0,              & \text{if } c \lt n\\
    1-\beta^{c-n},& \text{if } c \ge n
\end{cases}$$

!!! tip
    $\beta = 0.9$ is recommended to keep a meaningful $v_p$ value even with long array of values.

??? example "Example with a unknown maximum and evolution of the value with $\beta$"
    <div id="transparency_node_fct_unbounded" style="width:600px;height:250px;"></div>

<script>
    function property_value_unbounded(c, beta=0.9) {
        if (c <= 0)
            return 0;
        return 1-beta**c;
    }
    window.addEventListener('load', function() {
        // example for a property with min 2 values and infinity of values
        let x = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    	Plotly.newPlot(document.getElementById('transparency_node_fct_unbounded'), [
            {name: 'beta = 0.9', x: x,  y: x.map(x => property_value_unbounded(x)), mode: 'lines+markers'},
            {name: 'beta = 0.7', x: x,  y: x.map(x => property_value_unbounded(x, 0.7)), mode: 'lines+markers', line: {dash: 'dot', width: 2}},
            {name: 'beta = 0.5', x: x,  y: x.map(x => property_value_unbounded(x, 0.5)), mode: 'lines+markers', line: {dash: 'dot', width: 2}},
            {name: 'beta = 0.2', x: x,  y: x.map(x => property_value_unbounded(x, 0.2)), mode: 'lines+markers', line: {dash: 'dot', width: 2}},
            ],
        {
            margin: {t: 0},
            xaxis:{title: {text: `Example without upper boundary`}},
            yaxis:{title: {text: "property value"}},
            annotations: [
                {x: x.at(-1), y: property_value_unbounded(x.at(-1)),  text: 'converge to 1'},
            ]
        });
    }, false);
</script>

??? example "Example of the same VC type for different level of information"
    In the example below, we have a VC schema with 6 properties with different cardinality and 7 different VC instances, each with more or less defined values.
    <table>
    <thead>
    <tr>
        <th>property with<br>max cardinalty</th>
        <th style='min-width: 0;'>VC 1</th>
        <th style='min-width: 0;'>VC 2</th>
        <th style='min-width: 0;'>VC 3</th>
        <th style='min-width: 0;'>VC 4</th>
        <th style='min-width: 0;'>VC 5</th>
        <th style='min-width: 0;'>VC 6</th>
        <th style='min-width: 0;'>VC 7</th>
    </tr>
    </thead>
    <tbody id="example_ttn">
    </tbody>
    </table>
    <div id="transparency_node_examples" style="width:600px;height:250px;"></div>

<script>
    {
        let innerHTML = "";
        let ttns = [[], []]
        let cardinalities = [1, 1, 1, 2, '*', '*'];
        let exampleVC = [
            [0, 0, 0, 1, 0, 0],
            [0, 0, 1, 1, 0, 1],
            [1, 0, 1, 1, 0, 1],
            [1, 0, 1, 1, 1, 1],
            [1, 0, 1, 1, 2, 1],
            [1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 2, 6],
        ]
        for (let i = 0; i < cardinalities.length; ++i) {
            innerHTML += "<tr>";
            innerHTML += `<td> prop${i+1}, maxCard = ${cardinalities[i]}</td>`;
            for (let j = 0; j < exampleVC.length; ++j) {
                innerHTML += `<td>${exampleVC[j][i]}</td>`;
            }
            innerHTML += "</tr>";
        }
        innerHTML += "<tr>";
        innerHTML += '<td><math xmlns="http://www.w3.org/1998/Math/MathML"><msub><mi>T</mi><mrow data-mjx-texclass="ORD"><msub><mi>T</mi><mi>n</mi></msub></mrow></msub></math></td>';
        for (let j = 0; j < exampleVC.length; ++j) {
            innerHTML += "<td>";
            let w = 1 / cardinalities.length;
            let t = 0
            for (let i = 0; i < cardinalities.length; ++i) {
                t += w * (cardinalities[i] === '*' ? property_value_unbounded(exampleVC[j][i]) : property_value_bounded(exampleVC[j][i], cardinalities[i]))
            }
            ttns[0].push(`VC ${j+1}`); // used below to plot the values
            ttns[1].push(t); // used below to plot the values
            innerHTML += t.toFixed(3);
            innerHTML += "</td>";
        }
        innerHTML += "</tr>";
        document.querySelector("#example_ttn").innerHTML = innerHTML;
        window.addEventListener('load', function() {
            Plotly.newPlot(document.getElementById('transparency_node_examples'), [{x: ttns[0],  y: ttns[1], type: 'bar'}],
            {
                margin: {t: 0},
                yaxis:{title: {text: 'Tn index'}},
            });
        }, false);
    }
</script>



#### For the node of interest

Once all the $T_{T_n}$ values for each VC $n$ from the graph $G$ are calculated, an overall $T_T$ value is calculated taking into account the length of the path $d_n = dist(n_0, n)$ from the VC of interest $n_0$ to a VC $n$ in the graph $G$, with the principle that further away from $n_0$ is a VC, less impact it has on the overall $T_T$ index value.

If there are several VC at the same distance from $n_0$, the average $\overline{T_{T_d}}$ of the $T_{T_n}$ indexes at the distance $d$ is computed.

$$\exists n_0 \in G, \forall n \in G, T_T = \sum_{d_n} \gamma (1-\gamma)^{d_n-1}\overline{T_{T_d}}$$

??? info

    The above formula was built to normalise the index $T_T$ in the range $[0, 1]$.
    
    $$\text{for } 0 \le \gamma \le 1, \int_1^\infty \gamma (1-\gamma)^{x-1}dx = 1$$


!!! tip
    $\gamma = 0.5$ is recommended to have a balance between VC with and VC without linked VC.

??? example "Evolution of the value with $\gamma$"
    When $\gamma$ is close to 1, linked VC have a low impact on the overall Transparency index $T_T$.

    When $\gamma$ is close to 0, linked VC have a high impact on the overall Transparency index $T_T$.

    <div id="transparency_node" style="width:600px;height:250px;"></div>

<script>
    function tt_value(d, gamma=0.5) {
        return gamma*(1-gamma)**(d-1);
    }
    window.addEventListener('load', function() {
        let x = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    	Plotly.newPlot(document.getElementById('transparency_node'), [
            {name: 'gamma = 0.2', x: x,  y: x.map(x => tt_value(x, 0.2)), mode: 'lines+markers', line: {dash: 'dot', width: 2}},
            {name: 'gamma = 0.5', x: x,  y: x.map(x => tt_value(x, 0.5)), mode: 'lines+markers'},
            {name: 'gamma = 0.8', x: x,  y: x.map(x => tt_value(x, 0.8)), mode: 'lines+markers', line: {dash: 'dot', width: 2}},
            ],
        {
            margin: {t: 0},
            xaxis:{title: {text: `Example without upper boundary`}},
            yaxis:{title: {text: "property value"}}
        });
    }, false);
</script>

!!! example
    For the graph below, various transparency indexes $T_T$ are calculated for different values of $T_{T_n}$.

    ```mermaid
    flowchart TB
        service[Service Offering]
        res1[Resource 1]
        res2[Resource 2]
        res3[Resource 3]
        res4[Resource 4]

        service -- is composed of --> res1 & res2
        res1 -- is composed of --> res3 & res4
    ```
    <table id="example_tt">
    </table>
    <div id="transparency_examples" style="width:600px;height:250px;"></div>

<script>
    {
        let innerHTML = "";
        let tts = [[], []]
        let VCnames = ["Service Offering", "Resource 1", "Resource 2", "Resource 3", "Resource 4"];
        let dists = [1, 2, 2, 3, 3];
        let ttns = [
            [0.2, 0.0, 0.0, 0.0, 0.0],
            [0.2, 0.1, 0.1, 0.3, 0.9],
            [0.5, 0.1, 0.1, 0.3, 0.9],
            [0.5, 0.9, 0.8, 0.3, 0.1],
            [0.5, 0.9, 0.8, 0.3, 0.9],
        ]
        {
            innerHTML += '<thead><tr>';
            innerHTML += '<th></th>';
            for (let j = 0; j < ttns.length; ++j) {
                innerHTML += `<th>Example ${j+1}</th>`;
            }
            innerHTML += '</tr></thead>';
        }
        {
            innerHTML += '<tbody>';
            for (let i = 0; i < VCnames.length; ++i) {
                innerHTML += '<tr>';
                innerHTML += `<th>${VCnames[i]}<br>(d = ${dists[i]})</th>`;
                for (let j = 0; j < ttns.length; ++j) {
                    innerHTML += `<td>Tn=${ttns[j][i]}</td>`;
                }
                // since we have a simple graph, mean calculation are hardcoded
                innerHTML += '</tr>';
            }
            innerHTML += '<tr><th><math xmlns="http://www.w3.org/1998/Math/MathML"><msub><mi>T</mi><mi>T</mi></msub></math></th>';
            for (let j = 0; j < ttns.length; ++j) {
                let t = tt_value(1) * ttns[j][0]
                t += tt_value(2) * (ttns[j][1] + ttns[j][2]) / 2
                t += tt_value(3) * (ttns[j][3] + ttns[j][4]) / 2
                innerHTML += `<td>${t.toFixed(3)}</td>`;
                tts[0].push(`Example ${j+1}`); // used below to plot the values
                tts[1].push(t); // used below to plot the values
            }

            innerHTML += '</tr></tbody>';
        }
        document.querySelector("#example_tt").innerHTML = innerHTML;
        window.addEventListener('load', function() {
            Plotly.newPlot(document.getElementById('transparency_examples'), [{x: tts[0],  y: tts[1], type: 'bar'}],
            {
                margin: {t: 0},
                yaxis:{title: {text: 'T index'}},
            });
        }, false);

    }
</script>

### Composability

The Composability index $T_C$ is a function of two or more service descriptions, computing:

- the capacity of those services to be technically composed together, e.g.: software stack, compute, network and storage characteristics, plugin and extension configurations, …

This index is computed, for example, by instances of the Federated Catalogues, by analysing and comparing the characteristics of several service descriptions.

This index can be decomposed for each service description into several sub-functions:

- the level of detail of the [Software Bill of Material](https://www.cisa.gov/sbom)
- the list of known vulnerabilities such as the ones listed on the [National Vulnerability Database](https://nvd.nist.gov/).
- Intellectual Property rights via the analysis of the licenses and copyrights.
- the level of stickiness or adherence to specific known closed services
- the level of readiness for lift-and-shift migration.


### Semantic Match

The Semantic Match index $T_{SM}$ is a function of:

- the use of recommended vocabularies (Data Privacy Vocabulary, ODRL, …)
- the unsupervised classification of objects based on their properties
- Natural Language Processing and Large Language Model analysis from and to Domain Specific Language (DSL)
- Structured metadata information embedded in unstructured data containers (PDF/A-3a, …)


[^xacml]: PDP <https://www.oasis-open.org/committees/tc_home.php?wg_abbrev=xacml>
