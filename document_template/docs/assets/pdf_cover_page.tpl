<div>
    <div style="text-align: center; margin-top: 50px;">
        {% if config.site_name %}
            <h1 class="first-title" style="font-size: 36px; font-weight: bold;">{{ config.site_name }}</h1>
        {% endif %}
    </div>

    {% if config.theme.logo %}
    <div style="text-align: center;">
        <img alt="logo" src="../{{ config.theme.logo }}" style="display: block; width: 200px;">
    </div>
    {% endif %}

    <div style="text-align: center; margin-top: 10px; font-size: 20px; color: gray;">
        <table>

            {% if config.site_description %}
            <tr>
                <td>Description</td>
                <td>{{ config.site_description }}</td>
            </tr>
            {% endif %}

            {% if config.repo_url %}
            <tr>
                <td>Repository</td>
                <td><a href="{{ config.repo_url }}">{{ config.repo_url }}</a></td>
            </tr>
            {% endif %}

            {% if config.site_author %}
            <tr>
                <td>Author(s)</td>
                <td>{{ config.site_author }}</td>
            </tr>
            {% endif %}

            {% if config.copyright %}
            <tr>
                <td>Copyright(s)</td>
                <td>{{ config.copyright }}</td>
            </tr>
            {% endif %}
        </table>
    </div>
</div>
