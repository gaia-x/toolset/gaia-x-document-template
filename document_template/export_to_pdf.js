// Credits for script: https://github.com/majkinetor/mm-docs-template/blob/master/source/pdf/print.js
const puppeteer = require('puppeteer');
var args = process.argv.slice(2);
var url = args[0];
var pdfPath = args[1];

console.log('Saving', url, 'to', pdfPath);

// date –  formatted print date
// url  – document location
// pageNumber – current page number
// totalPages – total pages in the document
headerHtml = `<div style="border-bottom: solid 1px #bbb; border-image: linear-gradient(to right, #000071 0%, #46daff 100%) 1; width: 100%; font-size: 9px; color: #bbb; position: absolute; top: 0px; padding-top: 5px; padding-left: 5px;">
    <span class="title"></span>
</div>`;

footerHtml = `<div style="border-top: solid 1px #bbb; width: 100%; font-size: 9px; color: #bbb; border-image: linear-gradient(to right, #46daff 0%, #000071 100%) 1;">
    <div style="position: absolute; left: 5px;"><span class="date"></span></div>
    <div style="position: absolute; right: 5px;"><span class="pageNumber"></span>/<span class="totalPages"></span></div>
</div>`;

    (async() => {
    const browser = await puppeteer.launch({
        headless: true,
        executablePath: process.env.CHROME_BIN || '/usr/bin/chromium-browser',
        args: ['--no-sandbox', '--headless', '--disable-gpu', '--disable-dev-shm-usage']
    });

    const page = await browser.newPage();
    await page.goto(url, { waitUntil: 'networkidle0' });
    await page.pdf({
        path: pdfPath, // path to save pdf file
        format: 'A4', // page format
        displayHeaderFooter: true, // display header and footer (in this example, empty!)
        printBackground: true, // print background
        landscape: false, // use horizontal page layout
        headerTemplate: headerHtml, // indicate html template for header
        footerTemplate: footerHtml,
        scale: 0.8, //Scale amount must be between 0.1 and 2
        margin: { // increase margins (in this example, required!)
            top: 0,
            bottom: 0,
            left: 0,
            right: 0
        }
    });

    await browser.close();
})();
