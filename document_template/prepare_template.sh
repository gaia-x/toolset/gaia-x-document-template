#!/bin/sh
set -ex

rsync --ignore-existing --recursive -verbose /document_template/ .

j2 docs/assets/report_issue.js.j2 -o docs/assets/report_issue.js
j2 mkdocs.yml.j2 -o mkdocs.yml
cat -n mkdocs.yml
